FROM node:16-alpine

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe.js /

ENTRYPOINT ["node", "/pipe.js"]
