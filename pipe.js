const https = require('https')

/* Validate ENVs */
if (!process.env.API_KEY) {
  console.log('API_KEY is missing')
  process.exit(1)
}

/* If the token hasn't been provided */
if (!process.env.TOKEN) {
  if (!process.env.URL) {
    console.log('URL is missing')
    process.exit(1)
  }
  if (!process.env.STANDARD) {
    console.log('STANDARD is missing')
    process.exit(1)
  }
  if (!process.env.BREAKPOINTS) {
    console.log('BREAKPOINTS is missing')
    process.exit(1)
  }
}


/* Build data string */
if (!process.env.TOKEN) {
  var data = JSON.stringify({
    api_key: process.env.API_KEY,
    url: process.env.URL,
    standard: process.env.STANDARD,
    breakpoints: process.env.BREAKPOINTS ?? null,
    crawl: process.env.CRAWL ?? false,
    capture: process.env.CAPTURE ?? false
  })
} else {
  var data = JSON.stringify({
    api_key: process.env.API_KEY,
    token: process.env.TOKEN,
  })
}

const options = {
  hostname: 'accessibility.missionmade.digital',
  port: 443,
  path: '/api/v1/analysis-request',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': data.length,
  },
}

/* Make request */
const req = https.request(options, res => {
  res.on('data', d => {
    process.stdout.write(d)
  })
})
  .on('error', error => {
    console.error(error)
    process.exit(1)
  })

/* POST data */
req.write(data)
req.end()
