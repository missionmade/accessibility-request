# CI Pipe: Accessibility Request

This pipe will make an API request to the [Mission Made Accessibility Tool](https://accessibility.missionmade.digital) where the requested url will be crawled, analysed and reported back to your dashboard. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: docker://missionmade/accessibility-request:latest
    variables:
      API_KEY: "<string>"
      URL: "<boolean>"
      STANDARD: "<string>" # WCAG2A|WCAG2AA|WCAG2AAA
      BREAKPOINTS: "<string>" # 320x480 1024x768 1920x1080
      CRAWL: "<boolean>"
      CAPTURE: "<boolean>"
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*) | The API key unique to your account |
| URL (*) | The URL to crawl |
| STANDARD (*) | The URL to crawl |
| BREAKPOINTS (*) | A space separated string of dimensions to analyse. E.g. `"320x480 1024x768"`, `320x480 1024x768 1920x1080` |
| CRAWL | Determines if we should crawl sub pages of the provided URL. Defaults: `false` |
| CAPTURE | Determines if screengrabs should be taken. Defaults: `false`. |

_(*) = required variable._

## Alternatively, use the TOKEN method

Alternatively, you can offload the settings for the analyse by using the reports unique `token`.  Using a TOKEN, along with your `API_KEY` will trigger a report, based on the settings configured via your account.  

*This method will allow for other users of your account to configure the analyse settings, without the need to update the Pipeline configuration.*

```yaml
script:
  - pipe: docker://missionmade/accessibility-request:latest
    variables:
      API_KEY: "<string>"
      TOKEN: "<string>"
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*) | The API key unique to your account |
| TOKEN (*) | A unique string associated with your report. |

_(*) = required variable._


## Support

The pipeline and Accessibility API is currently undevelopment, so suggest to change. 

However, if you come across something you believe should be working please email <shughes@themission.co.uk>.
